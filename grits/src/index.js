import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import indexRoutes from "./routes/index";

import './index.css';
import * as serviceWorker from './serviceWorker';

const hist = createBrowserHistory();
ReactDOM.render(
    <div className="App">
        <header className="App-header">
          <Router history={hist}>
                <Switch>
                {indexRoutes.map((prop, key) => {
                    return <Route path={prop.path} key={key} component={prop.component} />;
                })}
                </Switch>
            </Router>
        </header>
      </div>

      , document.getElementById('root'));


serviceWorker.unregister();
