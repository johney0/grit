import LandingPage from "../views/LandingPage";
import QuestionPage from "../views/Question/QuestionPage.jsx";
import DashboardPage from "../views/Dashboard/DashboardPage.jsx";

let indexRoutes = [
  { path: "/question", name: "QuestionPage", component: QuestionPage },
  { path: "/dashboard", name: "DashboardPage", component: DashboardPage },
  { path: "/", name: "LandingPage", component: LandingPage }
];

export default indexRoutes;
