import React from "react";

import {
  XYPlot,
  XAxis,
  YAxis,
  VerticalGridLines,
  HorizontalGridLines,
  VerticalBarSeries,
  VerticalBarSeriesCanvas
} from 'react-vis';
import '../../../node_modules/react-vis/dist/style.css';

import Icon from 'react-icons-kit'
import { SideNav, Nav } from 'react-sidenav'
import { shoppingCart } from "react-icons-kit/fa/shoppingCart";
import { cubes } from "react-icons-kit/fa/cubes";
import { circleO } from "react-icons-kit/fa/circleO";
            
export default class DashboardPage extends React.Component{
  state = {
    useCanvas: false
};

    render(){
      const {useCanvas} = this.state;
    const BarSeries = useCanvas ? VerticalBarSeriesCanvas : VerticalBarSeries;
    return (
      <div>
       
        <XYPlot width={1000} height={300} stackBy="y">
          <VerticalGridLines />
          <HorizontalGridLines />
          <XAxis />
          <YAxis />
          <BarSeries data={[{x: 2, y: 10}, {x: 3, y: 5}, {x: 4, y: 15}, {x: 5, y: 15}, {x: 6, y: 15}, {x: 7, y: 15}, {x: 8, y: 15}, {x: 9, y: 15}]} />
          <BarSeries data={[{x: 2, y: 12}, {x: 3, y: 2}, {x: 4, y: 11}, {x: 5, y: 15}, {x: 6, y: 15}, {x: 7, y: 15}, {x: 8, y: 15}, {x: 9, y: 15}]} />
          <BarSeries data={[{x: 2, y: 16}, {x: 3, y: 7}, {x: 4, y: 9}, {x: 5, y: 15}, {x: 6, y: 15}, {x: 7, y: 15}, {x: 8, y: 15}, {x: 9, y: 15}]} />
        </XYPlot>
</div>
    )
    }
}