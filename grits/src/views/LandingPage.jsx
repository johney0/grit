import React from 'react';
import { Button,  Container, Row, Col  } from 'reactstrap'
import { Link } from 'react-router-dom'
import logo from '../logo.svg';
export default class LandingPage extends React.Component{
    render(){
        return(
            <Container>
                          <img src={logo} className="App-logo" alt="logo" />
                <Row>
                    <Col xs="6">
                            <Link style={{margin:'0 auto'}}  to = "/question">
                        <Button
                            color="rose"
                            round
                        >
                        Question
                        </Button>
                        </Link>
                    </Col>
                    <Col xs="6">
                            <Link style={{margin:'0 auto'}}  to = "/dashboard">
                        <Button
                            color="rose"
                            round
                        >
                        Dashboard
                        </Button>
                        </Link>
                    </Col>
                </Row>
               
            </Container>
        )
    }
}