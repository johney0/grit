import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, Container } from 'reactstrap'

export default class QuestionPage extends React.Component{
constructor(props){
    super(props);
    this.state={
        number:'',
        newValue:''
    }
}
    render(){
        return(
            <Container>
                <Form onSubmit={this.onSubmit}>
                <FormGroup>
                    <Input type="number" name="number" id="exampleText"  value ={this.state.number} onChange={this.handleInputChange}/>
                    </FormGroup>
                </Form>
                <div>{this.state.newValue}</div>
            </Container>
        )
    }
    
    handleInputChange=(e)=>{
        this.setState({[e.target.name] : e.target.value})
        console.log(this.state.number)
    }
onSubmit=(e)=>{
    e.preventDefault();
    let n = this.state.number

        let i = 0
        let reversed = 0
        let last
        while (i < 31) {
          last = n & 1
          n >>= 1
          reversed += last
          reversed <<= 1
          i++
        }
this.setState({newValue:reversed})
}
}

